class Country {
  late final String name;

  late final String flagUri;

  late final String code;

  late final String dialCode;

  Country(
      {required this.name,
      required this.code,
      required this.flagUri,
      required this.dialCode});
}
